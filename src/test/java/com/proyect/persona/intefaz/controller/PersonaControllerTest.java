package com.proyect.persona.intefaz.controller;

import com.proyect.persona.domain.entity.Persona;
import com.proyect.persona.domain.model.Imagen;
import com.proyect.persona.infrastructure.service.PersonaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class PersonaControllerTest {

    @InjectMocks
    PersonaController pController;

    @Mock
    PersonaService pService;

    Persona persona;

    static List<Imagen> imagenes;

    private static final int ID = 1;

    @BeforeEach
    void context(){
        MockitoAnnotations.openMocks(this);
        imagenes = new ArrayList<>();
        persona = new Persona();
        persona.setTelefono(332222);
        persona.setName("Prueba");
        persona.setId(1);

        Imagen imagen = new Imagen(new byte[1]);
        imagenes.add(imagen);
    }

    @Test
    void getPersonas() {
        List<Persona> personas = new ArrayList<>();
        personas.add(persona);
        when(pService.getAll()).thenReturn(personas);
        assertEquals(pController.getPersonas().getBody().size(), personas.size());
    }

    @Test
    void getPersona() {
        when(pService.getPersonaById(ID)).thenReturn(persona);
        assertEquals(pController.getPersona(ID).getStatusCode(), HttpStatus.OK);
    }

    @Test
    void savePersona() {
        assertEquals(pController.savePersona(persona).getStatusCode(), HttpStatus.OK);
    }

    @Test
    void deletePersona() {
        when(pService.delete(persona)).thenReturn(persona);
        assertEquals(pController.savePersona(persona).getStatusCode(), HttpStatus.OK);
    }

    @Test
    void deletePersonaById() {
        when(pService.deleteById(ID)).thenReturn(persona);
        assertEquals(pController.deletePersonaById(ID).getStatusCode(), HttpStatus.OK);
    }

    @Test
    void getImagenes() {
        when(pService.getPersonaById(Mockito.anyInt())).thenReturn(persona);
        when(pService.getImagenes(Mockito.anyInt())).thenReturn(imagenes);
        var result = pController.getImagenes(ID);
        assertEquals(result.getStatusCode(), HttpStatus.OK);
    }

    @Test
    void updatePersona() {
        when(pService.update(ID, persona)).thenReturn(persona);
        assertEquals(pController.updatePersona(ID, persona).getStatusCode(), HttpStatus.OK);
    }
}