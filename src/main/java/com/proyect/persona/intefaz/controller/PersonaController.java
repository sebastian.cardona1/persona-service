package com.proyect.persona.intefaz.controller;

import java.util.List;

import com.proyect.persona.domain.model.Imagen;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.proyect.persona.domain.entity.Persona;
import com.proyect.persona.infrastructure.service.PersonaService;

@RestController
@RequestMapping("/personas")
public class PersonaController {

	@Autowired
	private PersonaService pService;

	@Operation(summary = "Obtener todas las personas de la BD")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Personas encontradas", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Persona.class)) }),
			@ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
			@ApiResponse(responseCode = "404", description = "Personas no encontradas", content = @Content),
			@ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
	@GetMapping
	public ResponseEntity<List<Persona>> getPersonas(){
		
		List<Persona> personas = pService.getAll();
		if(personas.isEmpty()) {
			return ResponseEntity.noContent().build();
		}else {
			return ResponseEntity.ok(personas);
		}
	}

	@Operation(summary = "Obtener una persona de la DB por su id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Persona encontrada por su id", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Persona.class)) }),
			@ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
			@ApiResponse(responseCode = "404", description = "Personas no encontrada por su id", content = @Content),
			@ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
	@GetMapping("/{id}")
	public ResponseEntity<Persona> getPersona(@PathVariable("id") int id){
		
		Persona persona = pService.getPersonaById(id);
		if(persona!=null) {
			return ResponseEntity.ok(persona);
		}else {
			return ResponseEntity.notFound().build();
		}
		
	}

	@Operation(summary = "Guardar una persona de la BD")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Persona guardada correctamente", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Persona.class)) }),
			@ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
			@ApiResponse(responseCode = "404", description = "Persona no guardada correctamente", content = @Content),
			@ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
	@PostMapping
	public ResponseEntity<Persona> savePersona(@RequestBody Persona p){
		Persona persona = pService.save(p);
		return ResponseEntity.ok(persona);
	}

	@Operation(summary = "Eliminar una persona de la BD")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Persona eliminada correctamente", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Persona.class)) }),
			@ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
			@ApiResponse(responseCode = "404", description = "Personas no eliminada correctamente", content = @Content),
			@ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
	@DeleteMapping
	public ResponseEntity<Persona> deletePersona(@RequestBody Persona p){
		Persona persona = pService.delete(p);
		
		if(persona!=null) {
			return ResponseEntity.ok(persona);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	@Operation(summary = "Eliminar una persona de la DB por su id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Persona eliminada por su id correctamente", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Persona.class)) }),
			@ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
			@ApiResponse(responseCode = "404", description = "Personas no eliminada por su id correctamente", content = @Content),
			@ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
	@DeleteMapping("/{id}")
	public ResponseEntity<Persona> deletePersonaById(@PathVariable("id") int id){
		Persona persona = pService.deleteById(id);
		
		if(persona!=null) {
			return ResponseEntity.ok(persona);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	@Operation(summary = "Obtener las imagenes de una persona por su id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Imagenes de una persona obtenida correctamente", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Persona.class)) }),
			@ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
			@ApiResponse(responseCode = "404", description = "Imagenes de una persona no obtenidas", content = @Content),
			@ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
	@GetMapping("/imagenes/{personaId}")
	public ResponseEntity<List<Imagen>> getImagenes(@PathVariable("personaId") int personaId){
		Persona persona = pService.getPersonaById(personaId);
		if(persona!=null){
			List<Imagen> imagenes = pService.getImagenes(personaId);
			return ResponseEntity.ok(imagenes);
		}else{
			return ResponseEntity.notFound().build();
		}
	}

	@Operation(summary = "Actualizar a una persona por su id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Persona actualizada correctamente", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Persona.class)) }),
			@ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
			@ApiResponse(responseCode = "404", description = "Personas no actualizada correctamente", content = @Content),
			@ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
	@PatchMapping("/{id}")
	public ResponseEntity<Persona> updatePersona(@PathVariable("id") int id, @RequestBody Persona p){
		Persona persona = pService.update(id, p);
		if(persona!=null){
			return ResponseEntity.ok(persona);
		}else{
			return ResponseEntity.notFound().build();
		}
	}
}
