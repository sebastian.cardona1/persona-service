package com.proyect.persona.domain.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Imagen {
    private byte[] imagen;
}
