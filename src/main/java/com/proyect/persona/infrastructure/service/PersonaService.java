package com.proyect.persona.infrastructure.service;

import java.util.List;

import com.proyect.persona.domain.model.Imagen;
import com.proyect.persona.infrastructure.exceptions.NoDataFoundException;
import com.proyect.persona.infrastructure.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyect.persona.domain.entity.Persona;
import com.proyect.persona.infrastructure.repository.PersonaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@Service
public class PersonaService {
	@Autowired
	private PersonaRepository pRepository;

	@Autowired
	private RestTemplate restTemplate;

	public List<Imagen> getImagenes(int personaId){
		List<Imagen> imagenes = restTemplate.getForObject("http://localhost:8002/imagenes/persona/"+personaId,List.class);
		if(imagenes!=null){
			return imagenes;
		}else{
			throw new ResourceNotFoundException("Las imagenes asociadas al id "+personaId+" no se pudieron cargar correctamente");
		}
	}
	
	public List<Persona> getAll(){
		List<Persona> personas = pRepository.findAll();
		if (personas!=null){
			return personas;
		}else{
			throw new NoDataFoundException("Las personas no se pudieron cargar correctamente");
		}
	}
	
	public Persona getPersonaById(int id) {
		Persona persona = pRepository.findById(id).orElse(null);
		if (persona!=null){
			return persona;
		}else{
			throw new NoDataFoundException("La persona con id "+id+" no fue encontrada");
		}
	}

	@Transactional
	public Persona save(Persona persona) {
		Persona p = pRepository.save(persona);
		if(p!=null){
			return p;
		}else{
			throw new NoDataFoundException("No se pudo guardar a la persona correctamente");
		}
	}

	@Transactional
	public Persona delete(Persona p) {
		Persona persona = pRepository.findById(p.getId()).orElse(null);
		if(persona!=null) {
			pRepository.delete(p);
			return persona;
		}else {
			throw new NoDataFoundException("La persona a eliminar no fue encontrada");
		}
	}

	@Transactional
	public Persona deleteById(int id) {
		Persona persona = pRepository.findById(id).orElse(null);
		if(persona!=null) {
			pRepository.delete(persona);
			return persona;
		}else {
			throw new NoDataFoundException("La persona a eliminar con id "+id+" no fue encontrada");
		}
	}

	@Transactional
	public Persona update(int id, Persona p){
		Persona persona = pRepository.findById(id).orElse(null);
		if(persona!=null){
			persona.setName(p.getName());
			persona.setTelefono(p.getTelefono());
			pRepository.save(persona);
			return persona;
		}else{
			throw new NoDataFoundException("La persona a actualizar con id "+id+" no fue encontrada");
		}
	}
}
